FROM maven:3.5.2-jdk-8-slim AS MAVEN_BUILD
WORKDIR /build
COPY pom.xml .
RUN mvn dependency:go-offline
COPY /src ./src
RUN mvn spring-javaformat:apply
RUN mvn package

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/*.jar .
EXPOSE 8082
CMD java -jar *.jar